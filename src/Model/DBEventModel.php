<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            // TODO: Create PDO connection
            try{
            $this->db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8',
                          DB_USER, DB_PWD,
                          array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

        }catch(PDOException $e){
            echo "Something went wrong";
            echo $e->getMessage();
        }

      }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        $eventList = array();
        $stmt = $this->db->query('SELECT id, title, date, description FROM event ORDER BY id');
        try{
          $rows = $stmt->fetchAll();
      }catch(PDOException $e){
          echo "Something wrong happened";
          echo $e->getMessage();
      }

        foreach($rows as $row){
          $newEvent = new Event(
            $row['title'],
            $row['date'],
            $row['description'],
            $row['id']
          );
          $eventList[]=$newEvent;
        }

        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = null;
        try{
          Event::verifyId($id);
          $stmt = $this->db->prepare('SELECT id, title, date, description FROM event WHERE id = :id');
          $stmt->bindvalue(':id', $id);
          $affectedNo = $stmt->execute();
          $row = $stmt->fetch(PDO::FETCH_ASSOC);
          $event = new Event(
           $row['title'],
           $row['date'],
           $row['description'],
           $row['id']
          );
        }catch(PDOException $e){
          $e->getMessage();
          echo $error_message;
          exit();
        }

          return $event;
    }

    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
                  // TODO: Add the event to the database
        try{
            $event->verify(true);
            $stmt = $this->db->prepare('INSERT INTO event (title, date, description)'
                    . 'VALUES(:title, :date, :description)');
                    //$stmt->bindvalue(':id', $this->db->lastInsertId());
            $stmt->bindvalue(':title', $event->title);
            $stmt->bindvalue(':date', $event->date);
            $stmt->bindvalue(':description', $event->description);

        $stmt->execute();
            $event->id = $this->db->lastInsertId();

        }catch(PDOException $e){
          $e->getMessage();
          echo "Something happened";
          exit();
        }

      }

    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOExceptionsin
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // TODO: Modify the event in the database
        $stmt = $this->db->prepare('UPDATE event SET title = :title, date = :date, description = :description
        '.' WHERE id = :id');
        $stmt->bindvalue(':title', $event->title);
        $stmt->bindvalue(':date', $event->date);
        $stmt->bindvalue(':description', $event->description);
        $stmt->bindvalue(':id', $event->id);
        try{
          $event->verify();
          $stmt->execute();

        }catch(PDOException $e){
            $e->getMessage();
        }
    }

    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
        // TODO: Delete the event from the database

          try{
            Event::verifyId($id);
            $affectedNo = $this->db->exec('DELETE FROM event WHERE id=' . $id);
            return $affectedNo;

          }catch(PDOException $e){
            echo "Naa er det noe feeil ?";
            echo $e->getMessage();
          }

    }
}
